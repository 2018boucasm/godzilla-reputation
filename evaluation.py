import json

db = "databases/bdd1_red.json"

print("#########\nNOTEZ ENTRE 0, 1, 2\n#########")

with open(db, 'r+') as f:
    tweets = json.loads(f.read())
    try:
        i = 0
        for id in list(tweets.keys()):
            note = "42"
            i+=1
            while not (note.isdigit() and int(note) in [0,1,2]):
                print("NOTE NUMERO "+str(i))
                print(tweets[id]['text'])
                note = input('Note (0, 1, 2) : ')

            tweets[id]['note'] = (int(note)-1)
    except:
        pass

with open(db, 'w+') as f:
    f.write(json.dumps(tweets))
