import json
import numpy as np
from word2vec_transform import *

vocabulary = get_vocabulary_on_invers_index("databases/index_inverse_1.json")
WORDS = vocabulary.keys()
WORD_VECT_SIZE = 20

def create_input_data(filename):
    with open(filename,"r") as f:
        data = json.loads(f.read())

    final_matrix = np.zeros((WORD_VECT_SIZE,1))
    for key in data.keys():
        vect = np.zeros((WORD_VECT_SIZE,1))
        tweet = data[key]["text"]
        tweet = tweet.split(" ")
        for word in tweet:
            if word in WORDS:
                add_vect = vocabulary[word].reshape(WORD_VECT_SIZE,1)
                vect += add_vect
        final_matrix = np.concatenate((final_matrix,vect),axis=1)
    return final_matrix[:,1:]


def create_output_data(filename):
    """takes the filename of the dataset containing the notes and returns a (3,n_tweets)
    matrix containing the scores"""
    scores = {0:0,1:0,-1:0}
    with open(filename,"r") as f:

        data = json.loads(f.read())

    final_matrix = np.zeros((3,1))
    for key in data.keys():
        vect = np.zeros((3,1))
        score = data[key]["note"]
        vect[score+1,0]=1
        final_matrix = np.concatenate((final_matrix,vect),axis=1)
        scores[score]+=1

    print(scores)
    return scores,final_matrix[:,1:]

