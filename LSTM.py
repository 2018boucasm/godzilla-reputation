import json
import numpy as np
import re
from nltk.corpus import stopwords
import pandas as pd

import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords
from nltk.tokenize.casual import TweetTokenizer


import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from keras import Sequential
from keras.layers import Embedding,SpatialDropout1D,LSTM,Dense
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import EarlyStopping

# Importation des données:
with open("databases/bdd_emojis.json","r") as f:
    data = json.loads(f.read())

X = []
Y = []
for key in data.keys():
    X.append(data[key]['text'])
    Y.append(data[key]['note'])
X = np.array(X)

Y = np.array(Y)

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(stopwords.words('english'))

def treat_tweet(text):
    text = text.lower()
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub('', text)
    text = ' '.join(word for word in text.split() if word not in STOPWORDS)
    return text
treat_tweet = np.vectorize(treat_tweet)
X = treat_tweet(X)

vocabulary = []
MAX_SEQUENCE_LENGTH = 0
for x in X:
    tt = x.split(' ')
    if len(tt)>MAX_SEQUENCE_LENGTH:
        MAX_SEQUENCE_LENGTH = len(tt)
    for y in tt:
        if y not in vocabulary:
            vocabulary.append(y)

# The maximum number of words to be used. (most frequent)
MAX_NB_WORDS = len(vocabulary)
# Max number of words in each complaint.
#MAX_SEQUENCE_LENGTH =
# This is fixed. (size vector)
EMBEDDING_DIM = 25

tokenizer = Tokenizer(num_words=MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
tokenizer.fit_on_texts(X)
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

epochs = 10
batch_size = 64
output_length = 3


X = tokenizer.texts_to_sequences(X)
X = pad_sequences(X, maxlen=MAX_SEQUENCE_LENGTH)
print('Shape of data tensor:', X.shape)

Y = pd.get_dummies(Y)
print('Shape of label tensor:', Y.shape)



X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.10, random_state = 42)
print(X_train.shape,Y_train.shape)
print(X_test.shape,Y_test.shape)

epochs = 10
batch_size = 32

with open('databases/glove_en_voc.json','r') as f:
    embedding_index = json.loads(f.read())

# vocab_size = len(embedding_index.keys())

embedding_matrix = np.zeros((MAX_NB_WORDS, EMBEDDING_DIM))
for word, i in tokenizer.word_index.items():
    embedding_vector = embedding_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = np.array(embedding_vector)


try:
    print(model.name)
    print(model.summary())
except:
    model = Sequential()
    model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1], weights=[embedding_matrix]))
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(EMBEDDING_DIM, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    history = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size,validation_split=0.1,callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])



accr = model.evaluate(X_test,Y_test)
print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))

plt.subplot(121)
plt.title('Loss')
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()

plt.subplot(122)
plt.title('Accuracy')
plt.plot(history.history['acc'], label='train')
plt.plot(history.history['val_acc'], label='test')
plt.legend()
plt.show();


def evaluate(tweet):
    seq = tokenizer.texts_to_sequences(tweet)
    padded = pad_sequences(seq, maxlen=MAX_SEQUENCE_LENGTH)
    pred = model.predict(padded)

