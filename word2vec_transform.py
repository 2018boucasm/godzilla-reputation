from gensim.test.utils import common_texts, get_tmpfile
from gensim.models import Word2Vec
import json
import re
import numpy as np


def get_vocabulary(filename):
    data={}
    with open(filename,"r") as f:
        data = json.loads(f.read())

    all_words = ""
    for key in data.keys():
        text = re.sub(r'\W+',"", data[key]['text'],re.IGNORECASE)
        all_words = all_words + text +" "
    all_words = all_words.replace('\n',' ').lower()
    all_words = all_words.split(' ')


    word2vec = Word2Vec([all_words], min_count=1,size=100)
    voc = {}
    for word in all_words:
        if word not in voc.keys():
            voc[word]=word2vec.wv[word]
    return voc

def get_vocabulary_on_invers_index(filename):
    data={}
    with open(filename,"r") as f:
        data = json.loads(f.read())

    all_words = ""
    for key in data.keys():
        text = re.sub(r'\W+',"", key,re.IGNORECASE)
        all_words = all_words + text +" "
    all_words = all_words.replace('\n',' ').lower()
    all_words = all_words.split(' ')


    word2vec = Word2Vec([all_words], min_count=1,size=20)
    voc = {}
    for word in all_words:
        if word not in voc.keys():
            voc[word]=word2vec.wv[word]
    return voc
