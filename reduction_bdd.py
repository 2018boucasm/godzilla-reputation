import json
import re

data = {}
with open("databases/bdd_extended.json","r") as f:
    contenu = json.loads(f.read())
    for tweet in contenu:
        texte = tweet['full_text']
        text_sans_url = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', texte, flags=re.MULTILINE)
        text_sans_url = re.sub(r'@[\w]*(Godzilla)[\w]*',"", text_sans_url,re.IGNORECASE)
        if "godzilla" in text_sans_url.lower():
            content = {
            "lang":tweet['metadata']['iso_language_code'],
            "id":tweet['id'],
            "text":text_sans_url,
            "retweet_count":tweet['retweet_count'],
            "favorite_count":tweet['retweet_count']
            }
            data[tweet['id']]=content
    print(len(data))

with open('databases/bdd_red.json',"w") as f:
    f.write(json.dumps(data,indent=4))

"""
Ce qui est important:
-id
-text
-retweet_count
-favorite_count

"""
