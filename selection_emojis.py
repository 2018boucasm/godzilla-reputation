import json
from nltk.corpus import stopwords
from nltk.tokenize.casual import TweetTokenizer
import re

stop_words = set(stopwords.words('english'))
stop_words_theme = ["Godzilla"]
for i,x in enumerate(stop_words_theme):
    stop_words_theme[i]=x.lower()

pos_words = ["king","happy","best","awesome","great","monster","badass","fucking","fuckin","fuck","fire","more"]
neg_words = ['junk',"longest"]
filtre = ["-","$",",","'",'’','*',"“","‘","|","〝","—","✏","•","《","》","🦋",", '”","£","”"]
smiley_pos=['😊', '👍', '🤩', '💕', '🐲', '😅', '😂', '🙌', '😍', '💗', '❤', '😎', '💛', '🧡', '💙', '🖤', '😏', '☺', '🐉', '🏆', '😆', '🦖', '🦕', '🐛', '👏', '💖', '😘', '👌', '💞', '🐊', '💥', '🔥', '⭐', '😀', '💪', '🤘', '😁', '🙂', '🦎', '✌', '✨', '🤣', '😇', '🙉', '😱', '🤷', '💜', '🐸', '😃', '🥰', '💚', '💝', '💘', '💓', '🤡', '✔', '♥', '<3', '🤗', '😉', '🤓', '🤪', '🙃', '🥺', '✅', '😮', '🌟', '😵', '🤞', '🥳', '🤜', '😄', '★', '😌', '♡', '😗', '😋', '🤭', '😙']
smiley_neg=['🤔', '😒', '😤', '😢', '😞', '😬', '😫', '😭', '🤦', '😛', '😝', '😔', '🚫', '🤧', '💀', '🤢', '😪', '😩', '😜', '🤕', '🤨', '😡', '👎', '😕', '😑', '💔', '⛈', '☹', '😈', '🤐']
eles=[]

def change_token(token):
    token = token.lower()
    if len(token)==0: return None
    if token in filtre: return None
    if re.match(r'[\77\100"&+=,.:;/!%…]+', token,re.IGNORECASE)!=None or re.match(r"['\(\)]", token,re.IGNORECASE)!=None: return None
    if token=="rt": return None
    if token[0]=="#":
        token=token[1:]
    elif token[0]=="@":
        token=None
    if re.match(r"\W",token,re.IGNORECASE) and not token in smiley_pos+smiley_neg:
        if token not in eles:
            eles.append(token)
    return token

def tokenize_data(dict):
    new_dict = {}
    print(type(data))
    for key in dict.keys():
        tokens = TweetTokenizer().tokenize(dict[key]['text'].lower())

        new_tokens = []
        for word in tokens:
            nword = change_token(word)
            if nword == None:
                continue
            if nword not in stop_words and nword not in stop_words_theme:
                new_tokens.append(nword)
        new_dict[key]=new_tokens

    return new_dict


with open("databases/bdd_red.json","r") as f:
    data = json.loads(f.read())

data_tokens = tokenize_data(data)

qte = [0,0,0]
cles = list(data.keys())[:]
for key in cles:
    score = 0
    for token in data_tokens[key]:
        if token in smiley_pos:
            score+=1
        elif token in smiley_neg:
            score-=1
    if score>0:
        data[key]['note']=1
        qte[0]+=1
    elif score<0:
        data[key]['note']=-1
        qte[1]+=1
    else:
        data[key]['note']=0

        if qte[2]>2000:
            del(data[key])
        else:
            qte[2]+=1

with open("databases/bdd_emojis.json","w") as f:
    f.write(json.dumps(data,indent=4))

i=0
with open("databases/bdd_emojis_sans.json","w") as f:
    for key in data.keys():
        txt = data[key]['text']
        txt1 = data[key]['text']

        for smiley in smiley_pos+smiley_neg:
            txt=txt.replace(smiley,"")

        data[key]['text'] = txt
    f.write(json.dumps(data,indent=4))


def note_smiley():
    for ele in eles:
        print('\n\n')
        print("|",ele,"|")
        a = input("Votre note ?")
        while a not in ["1","0","2"]:
            a = input("Votre note ?")
        if a == "0":
            smiley_neg.append(ele)
        elif a == "2":
            smiley_pos.append(ele)

