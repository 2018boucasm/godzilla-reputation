import json
import nltk
from nltk.tokenize import word_tokenize
import matplotlib.pyplot as plt

data = json.load(open('databases/bdd_apprentissage.json'))
vocabulary = json.load(open('databases/en_vocabulary.json'))
feelings = vocabulary.keys()

def english_text_selection(data):
    new_dict = {}
    for id in data.keys():

        if data[id]['lang'] == 'en':
            new_dict[id] = data[id]['text']

    return new_dict

clean_data = english_text_selection(data)

def tokenize_data(dict):
    new_dict = {}
    for id in dict.keys():
        new_dict[id] = word_tokenize(dict[id])
    return new_dict

token_dict = tokenize_data(clean_data)



def calculate_score(data):
    new_dict = {}

    for id in data.keys():
        score = 0
        list = data[id]
        for word in list:
            down_word = word.lower()
            if word.lower() in feelings:
                score += vocabulary[down_word]

        new_dict[id] = score

    return new_dict

tweet_scores = calculate_score(token_dict)


def show_scores(donnees):
    scores = {"positive": 0, "negative": 0, "neutral": 0}
    reussi = 0
    total = 0
    for id in donnees.keys():
        vrai_cluster = data[id]["note"]+1
        if donnees[id]>0:
            scores["positive"]+=1
            cluster_predit=2
        elif donnees[id]<0:
            scores["negative"]+=1
            cluster_predit=0
        else:
            scores["neutral"]+=1
            cluster_predit=1
        total += 1
        if cluster_predit == vrai_cluster: reussi +=1
    x = ["positive","negative","neutral"]
    y = [scores[key] for key in x]
    print("Efficacité : {}".format(y[0]/(y[0]+y[1])))
    print("Accuracy : {}".format(reussi/total))
    plt.bar(x, y)
    # We can set the number of bins with the `bins` kwarg

    plt.show()

show_scores(tweet_scores)