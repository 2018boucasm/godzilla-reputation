import json
import codecs
import numpy as np

print('Indexing word vectors.')

embeddings_index = {}
f1 = open('glove_twitter/glove.twitter.27B.25d.txt', encoding='utf-8')
# f2 = open('glove_twitter/glove.twitter.27B.50d.txt', encoding='utf-8')
# f3 = open('glove_twitter/glove.twitter.27B.100d.txt', encoding='utf-8')
# f4 = open('glove_twitter/glove.twitter.27B.200d.txt', encoding='utf-8')
for line in f1:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f1.close()

# print(embeddings_index)
embedding_words = embeddings_index.keys()
print(embeddings_index['horacas'].shape)
print('Found %s word vectors.' % len(embeddings_index))

with open('databases/index_inverse_1.json',"r") as f:
    tweet_words = json.loads(f.read()).keys()

# print(tweet_words)

tweet_voc = {}
for word in tweet_words:
    if word in embedding_words:
        tweet_voc[word] = embeddings_index[word].tolist()

# print(tweet_voc)
print('Found %s word vectors.' % len(tweet_voc.keys()))

json.dump(tweet_voc, codecs.open('databases/glove_en_voc.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)

