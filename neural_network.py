import json
from word2vec_transform import *
from input_creator import *
import numpy as np
import keras
import sklearn
import matplotlib
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Dense
from keras.optimizers import Adam
from keras import regularizers
from keras.callbacks import EarlyStopping
from keras.datasets import mnist
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec




lambda2=0.0001
WORD_VECT_SIZE = 20
RAPPORT = 1


input_data = create_input_data("databases/bdd_emojis_sans.json").T
output_data = create_output_data("databases/bdd_emojis_sans.json")[1].T

print(input_data.shape,output_data.shape)

def create_balanced_data(input_data,output_data):
    n, m = input_data.shape
    final_input = np.zeros((1,WORD_VECT_SIZE))
    final_output = np.zeros((1,3))
    for i in range(n):
        if output_data[i,0]:
            input_vect = input_data[i,:].reshape((1,WORD_VECT_SIZE))
            output_vect = output_data[i,:].reshape((1,3))
            final_input = np.concatenate((final_input,input_vect),axis=0)
            final_output = np.concatenate((final_output,output_vect),axis=0)
    final_input = final_input[1:,:]
    final_output = final_output[1:,:]
    neg_number = final_input.shape[0]
    pos_number, neut_number = 0, 0
    i = 0

    while pos_number < neg_number*RAPPORT or neut_number < neg_number*RAPPORT or i < input_data.shape[0]:
        # print(i)
        input_vect = input_data[i,:].reshape((1,WORD_VECT_SIZE))
        output_vect = output_data[i,:].reshape((1,3))
        if pos_number < neg_number*RAPPORT and output_vect[0,2]:
            final_input = np.concatenate((final_input,input_vect),axis=0)
            final_output = np.concatenate((final_output,output_vect),axis=0)
            pos_number += 1
        if neut_number < neg_number*RAPPORT and output_vect[0,1]:
            final_input = np.concatenate((final_input,input_vect),axis=0)
            final_output = np.concatenate((final_output,output_vect),axis=0)
            neut_number += 1
        i+=1
    return final_input,final_output

# input_data, output_data = create_balanced_data(input_data, output_data)
# print(input_data.shape,output_data.shape)

X_train, X_test, Y_train, Y_test = train_test_split(input_data, output_data, test_size=0.1, random_state=None)
print(X_train.shape,Y_train.shape)
# X_val, X_test, Y_val, Y_test = train_test_split(X_test, Y_test, test_size=0.5, random_state=None)
# print(X_val.shape,Y_val.shape)
# print(Y_train[:7,:])
# print(Y_val[:7,:])
# print(Y_test[:7,:])

model = Sequential()
model.add(Dense(10, activation="relu", kernel_regularizer = regularizers.l2(lambda2), input_shape=(WORD_VECT_SIZE,)))
# model.add(Dense(7, activation="relu", kernel_regularizer = regularizers.l2(lambda2)))
# model.add(Dense(6, activation="relu", kernel_regularizer = regularizers.l2(lambda2)))
# model.add(Dense(6, activation="relu", kernel_regularizer = regularizers.l2(lambda2)))
model.add(Dense(3, activation="sigmoid", kernel_regularizer = regularizers.l2(lambda2), name="representation2"))
model.compile(loss="categorical_crossentropy", optimizer='Adam',metrics=["accuracy"])
results = model.fit(X_train, Y_train, batch_size=50, epochs=1000,callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)],
                            verbose=1, validation_split=0.1)
# mean_squared_error




accr = model.evaluate(X_test,Y_test)
print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))


X_total = create_input_data("databases/bdd_red.json").T
print(X_total.shape)
total_results = model.predict(X_total)
print(total_results.shape)

def note_computation(prediction):
    sums = prediction.sum(axis=0)
    total= sum(sums)

    return 100/total*sums


print(note_computation(total_results))

def pos_neg(trio):
    total = trio[0] + trio[2]
    list = np.array([trio[0],trio[2]])
    return list/total

print(pos_neg(note_computation(total_results)))
