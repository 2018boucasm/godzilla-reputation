from textblob import TextBlob, Word, Blobber
import json



def find_negative_tweets(filename,seuil):
    tweets_sentiments = {}
    #On génère les données
    with open(filename, 'r+') as f:
        tweets = json.loads(f.read())
        i = 0
        for tweet in list(tweets.values()):
            if tweet['lang'] == "en" and i > seuil: #Langue. Fr ou En.

                texte = tweet['text']
                tweet_sentiment = (TextBlob(texte)).sentiment

                if (tweet_sentiment.polarity < 0 and tweet_sentiment.subjectivity != 0): # On évite les tweets neutres

                    tweets_sentiments[tweet['id']] = {'text': texte, 'polarity': tweet_sentiment.polarity, 'rt': tweet['retweet_count'], 'fav': tweet['favorite_count']}
            i+=1
    with open("databases/bdd_red_neg.json", 'w') as f:
        f.write(json.dumps(tweets_sentiments))
    return tweets_sentiments, len(tweets_sentiments.keys())


print(find_negative_tweets("databases/bdd_red.json", 2000)[1])


