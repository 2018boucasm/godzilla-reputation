import json

db = "databases/bdd_red.json"

t1 = {}
t2 = {}
t3 = {}
t4 = {}

with open(db, 'r+') as f:
    tweets = json.loads(f.read())
    i = 0
    for id in list(tweets.keys()):
        i+= 1
        if i > 1000:
            if i <= 1250:
                t1[id] = tweets[id]
            elif i <= 1500:
                t2[id] = tweets[id]
            elif i <= 1750:
                t3[id] = tweets[id]
            elif i <= 2000:
                t4[id] = tweets[id]
        

with open("databases/bdd1_red.json", 'w+') as f:
    f.write(json.dumps(t1))

with open("databases/bdd2_red.json", 'w+') as f:
    f.write(json.dumps(t2))

with open("databases/bdd3_red.json", 'w+') as f:
    f.write(json.dumps(t3))

with open("databases/bdd4_red.json", 'w+') as f:
    f.write(json.dumps(t4))