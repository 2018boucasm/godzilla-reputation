import numpy as np
from word2vec_transform import get_vocabulary
import json
import matplotlib.pyplot as plt
import re

K=3
# On va charger les tweets et les passer en mode vectoriel

data = {}
with open("databases/bdd_apprentissage.json") as f:
    data = json.loads(f.read())
data_apprentissage = data

vocabulaire = get_vocabulary("databases/bdd_red.json")

# On transforme chaque tweet en liste de vecteurs into somme

for key in data.keys():
    text = re.sub(r'\W+',"", data[key]['text'],re.IGNORECASE).lower().replace('\n'," ").split(' ')
    list_vector=[]
    for word in text:
        vecteur = vocabulaire[word]
        list_vector.append(vecteur)
    data[key]['vecteur']=sum(list_vector)

# Mise en forme des données
X = []
for key in data.keys():
    X.append(data[key]['vecteur'])

"""
plt.figure()

for key in data.keys():
    if i>=max: break
    i+=1
    plt.plot(data[key]['vecteur'][0],data[key]['vecteur'][1],marker="+")
plt.show()
"""
from sklearn.cluster import KMeans
kmeans = KMeans(n_clusters=K, random_state=0).fit(X)
#print(kmeans.labels_)

# On stocke dans bons clusters

clusters = [[] for i in range(K)]
for i,label in enumerate(kmeans.labels_):
    clusters[label].append(list(data_apprentissage.keys())[i])

"""
for i in range(K):
    print("================================\n\n Cluster {}".format(i))
    for j in range(20):
        print(clusters[i][j])
"""

data_apprentissage = {}
with open("databases/bdd_apprentissage.json") as f:
    data_apprentissage = json.loads(f.read())

correspondance = []
reussi = 0
total = 0
Positif = 0
Negatif = 0

proportions_apprentissage = [0 for i in range(K)]
for key in data_apprentissage.keys():
    proportions_apprentissage[data_apprentissage[key]["note"]+1]+=1

for cluster in clusters:
    scores=[0 for i in range(K)]
    for key in cluster:
        if key in data_apprentissage.keys():
            note = data_apprentissage[key]["note"]+1
            scores[note]+=1
    print(scores)
    proportions = [scores[i]/proportions_apprentissage[i] for i in range(K)]
    cluster_correspondant = proportions.index(max(proportions))
    print("Proportions : ",proportions)
    print(cluster_correspondant)
    if cluster_correspondant == 2: Positif+=max(scores)
    if cluster_correspondant == 0: Negatif+=max(scores)
    reussi += max(scores)
    total +=len(cluster)
    correspondance.append(cluster_correspondant)
print("Efficacité relative (soyons honnêtes, c'est probablement pas terrible)\n {}".format(reussi/total))
print("Ratio Positif/(Positif+Négatif) = {}".format(Positif/(Positif+Negatif)))

print("\n\n\n")
## Essayons une autre méthode de répartition
correspondance = []
reussi = 0
total = 0
Positif = 0
Negatif = 0


Y1 = []
Y2 = []
Y3 = []


for cluster in clusters:
    scores=[0 for i in range(K)]
    for key in cluster:
        if key in data_apprentissage.keys():
            note = data_apprentissage[key]["note"]+1
            scores[note]+=1


    print("Scores : ",scores)
    Y1.append(scores[0])
    Y2.append(scores[1])
    Y3.append(scores[2])

    proportions = [scores[i]/proportions_apprentissage[i] for i in range(K)]
    cluster_correspondant = proportions.index(max(proportions))
    print("Proportions : ",proportions)
    while cluster_correspondant in correspondance:
        print(proportions,correspondance)
        del(proportions[cluster_correspondant])
        cluster_correspondant = proportions.index(max(proportions))

    print("Cluster correspondant : ",cluster_correspondant)
    if cluster_correspondant == 2: Positif+=max(scores)
    if cluster_correspondant == 0: Negatif+=max(scores)
    reussi += max(scores)
    total +=len(cluster)
    correspondance.append(cluster_correspondant)
print("Efficacité relative (soyons honnêtes, c'est probablement pas terrible)\n {}".format(reussi/total))
print("Ratio Positif/(Positif+Négatif) = {}".format(Positif/(Positif+Negatif)))



X = np.arange(K)
width = 0.27
plt.title("Répartition sur les données labellisées")
ax = plt.subplot(111)
rect1 = ax.bar(X, Y1, width=width, color='b', align='center',label="Négatif")
rect2 = ax.bar(X+width, Y2, width=width, color='g', align='center',label="Neutre")
rect3 = ax.bar(X+2*width, Y3, width=width, color='r', align='center',label="Positif")


ax.set_xticks(X+width)
#ax.set_xticklabels( ('2011-Jan-4', '2011-Jan-5', '2011-Jan-6') )
ax.set_xticklabels( ('Cluster 1', 'Cluster 2', 'Cluster 3') )
ax.legend( (rect1[0], rect2[0], rect3[0]), ('Négatif', 'Neutre', 'Positif') )
def autolabel(rects):
    for rect in rects:
        h = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2., 1.05*h, '%d'%int(h),
                ha='center', va='bottom')

autolabel(rect2)
autolabel(rect1)
autolabel(rect3)
plt.show()


