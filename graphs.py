import numpy as np
import matplotlib.pyplot as plt


N = 6
positive = (88, 64, 67, 50, 73, 83)
negative = (12, 36, 33, 50, 27, 17)
ind = np.arange(N)    # the x locations for the groups
width = 0.35       # the width of the bars: can also be len(x) sequence

p1 = plt.bar(ind, positive, width, color="green")
p2 = plt.bar(ind, negative, width,
             bottom=positive, color="orange")

plt.ylabel('Scores')
plt.title('Score de positif négatif')
plt.xticks(ind, ('Rotten tomatoes', 'TextBlob', 'Pondération', 'Pondération++', 'Non supervisé', 'Supervisé'))
plt.yticks(np.arange(0, 100, 10))
plt.legend((p1[0], p2[0]), ('positive', 'negative'))

plt.show()