import json
import nltk
from nltk.corpus import stopwords
from nltk.tokenize.casual import TweetTokenizer
import matplotlib.pyplot as plt
import re
import numpy as np

stop_words = set(stopwords.words('english'))
stop_words_theme = ["Godzilla"]
for i,x in enumerate(stop_words_theme):
    stop_words_theme[i]=x.lower()

pos_words = ["king","happy","best","awesome","great","monster","badass","fucking","fuckin","fuck","fire","more"]
neg_words = ['junk',"longest"]
couples_mots = [["long","live"],["king","monster"],["king","monsters"]]
smiley_pos=["😊","👍","🤩","💕","🐲","😅","😂","🙌","😍","💗","❤","😎","💛","🧡","💙","🖤","😏","☺","🐉","🏆","😆","🦖","🦕","🐛","👏","💖","😘","👌","💞","🐊",'💥',"🔥","epic","favorite","like","cool"]
smiley_neg=["🤔","😒","😤","😢"]
filtre = ["-","$",",","'",'’','*',"“","‘","|","〝","—","✏","•","《","》","🦋",", '”","£","”"]

inversion = ['but']


data = json.load(open('databases/bdd_apprentissage.json'))
vocabulary = json.load(open('databases/en_vocabulary.json'))


for voc in pos_words:
    vocabulary[voc]=2
for voc in neg_words:
    vocabulary[voc]=2
for pos_words in smiley_pos:
    vocabulary[voc]=2
for pos_words in smiley_neg:
    vocabulary[voc]=-2
feelings = vocabulary.keys()

def change_token(token):
    token = token.lower()
    if len(token)==0: return None
    if token in filtre: return None
    if re.match(r'[\77\100"&+=,.:;/%…]+', token,re.IGNORECASE)!=None or re.match(r"['\(\)]", token,re.IGNORECASE)!=None: return None
    if token=="rt": return None
    if token[0]=="#":
        token=token[1:]
    elif token[0]=="@":
        token=None
    if re.match(r"\W",token,re.IGNORECASE) and not token in smiley_pos+smiley_neg:
        eles.append(token)
    return token

def english_text_selection(data):
    # On sélectionne que les tweets en anglais
    new_dict = {}
    notes = {}
    for id in data.keys():
        if data[id]['lang'] == 'en':
            new_dict[id] = data[id]['text']
            notes[id] = data[id]["note"]+1

    return new_dict,notes

clean_data,notes = english_text_selection(data)

def tokenize_data(dict):
    new_dict = {}
    for id in dict.keys():
        tokens = TweetTokenizer().tokenize(dict[id].lower())

        new_tokens = []
        for word in tokens:
            nword = change_token(word)
            if nword == None:
                continue
            if nword not in stop_words and nword not in stop_words_theme:
                new_tokens.append(nword)
        new_dict[id]=new_tokens

    return new_dict

token_dict = tokenize_data(clean_data)

def calculate_score_word(word):
    score = 0
    if word in feelings:
        score += vocabulary[word]
    #if word in smiley_pos: score += 2
    #if word in smiley_neg: score -=2

   # if "!" in word : score*=1.5
    return score

def calculate_score_tweet(tweet):
    score =0
    expli = {}
    inverted = False
    for word in tweet:
        d=calculate_score_word(word)
        score+=d
        expli[word]=d

        if word in inversion:
            score*=-1
            inverted = True

    if inverted:
        expli['BUT'] = "Inversion"
        print(tweet)
    for couple in couples_mots:
        for word in couple:
            if word not in tweet:
                break
        else:
            score+=5
    return score,expli


def calculate_score(data):
    new_dict = {}
    new_dict_expli = {}

    for key in data.keys():
        d = calculate_score_tweet(data[key])
        new_dict[key] = d[0]
        new_dict_expli[key] = d[1]

    return new_dict,new_dict_expli

tweet_scores,tweet_scores_expli = calculate_score(token_dict)


def show_scores(tweets_score,notes,tweet_scores_expli):
    reussi = 0
    total = 0
    positif = 0
    negatif = 0
    nbr_positifs = 0
    nbr_negatifs = 0

    offset = 0.2

    Y=np.array([[0 for i in range(3)] for j in range(3)],dtype=float)
    for key in tweets_score:
        total += 1
        vrai_cluster = notes[key]
        if vrai_cluster == 0: nbr_negatifs+=1
        elif vrai_cluster == 2: nbr_negatifs+=1
        if tweets_score[key]>offset:
            positif += 1
            cluster_predit = 2
        elif tweet_scores[key]<-offset:
            negatif += 1
            cluster_predit = 0
        else:
            cluster_predit = 1
        if cluster_predit == vrai_cluster: reussi +=1
        elif cluster_predit==2 and vrai_cluster==0 : print("Cluster prédit: {}, cluster réel: {}, score du tweet: {}\n{}\nExplication : {}\n\n".format(cluster_predit,vrai_cluster,tweets_score[key],data[key]['text'],tweet_scores_expli[key]))

        Y[cluster_predit][vrai_cluster]+=1
    sommes = [sum(Y[0]),sum(Y[1]),sum(Y[2])]

    print(positif,negatif)
    efficacite = positif/(positif+negatif)
    accuracy = reussi/total
    print(sommes)
    print("Efficacité : {}\n Accuracy : {}".format(efficacite,accuracy))
    return x,Y[:,0],Y[:,1],Y[:,2],sommes


X,Y1,Y2,Y3,sommes = show_scores(tweet_scores,notes,tweet_scores_expli)




X = np.arange(3)
width = 0.27
plt.title("Répartition sur les données labellisées")
ax = plt.subplot(111)
rect1 = ax.bar(X, Y1, width=width, color='b', align='center',label="Négatif")
rect2 = ax.bar(X+width, Y2, width=width, color='g', align='center',label="Neutre")
rect3 = ax.bar(X+2*width, Y3, width=width, color='r', align='center',label="Positif")


ax.set_xticks(X+width)
#ax.set_xticklabels( ('2011-Jan-4', '2011-Jan-5', '2011-Jan-6') )
ax.set_xticklabels( ('Prédit Négatif', 'Prédit Neutre', 'Prédit Positif') )
ax.legend( (rect1[0], rect2[0], rect3[0]), ('Réel Négatif', 'Réel Neutre', 'Réel Positif') )
def autolabel(rects,somme):
    for rect in rects:
        h = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2., 1.03*h, '%d'%int(h),
                ha='center', va='bottom')

autolabel(rect2,sommes[0])
autolabel(rect1,sommes[1])
autolabel(rect3,sommes[2])
plt.show()


def search(text):
    for key in data.keys():
        if text in data[key]['text']:
            print(data[key]['text'])
            print(tweet_scores_expli[key])
            print("\n\n")

