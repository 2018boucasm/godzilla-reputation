import json

def create_voc(filename):
    """the file must have at least one line"""
    new_dict = {}

    with open(filename) as file:
        line = file.readline()
        line = line.split('\t')
        while len(line)!=1:
            new_dict[line[0]] = int(line[1])
            line = file.readline()
            line = line.split('\t')
    return new_dict


# print(create_voc("vocabulary.txt"))
with open('databases/fr_vocabulary.json',"w") as f:
    f.write(json.dumps(create_voc("vocabulary_fr.txt"), indent=4))
