import tweepy,json
from secrets import consumer_key,consumer_secret,access_token,access_token_secret

id_max = "1140356004657475584"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

isApiOK = True

while isApiOK:
    try:
        with open('databases/bdd_extended.json', "r") as f:

            tweets = json.loads(f.read())
            list_ids = []
            for tweet in tweets:
                list_ids.append(tweet['id'])
            if len(list_ids)==0:
                id_max = "1140356004657475584"
            else:
                id_max = str(int(min(list_ids))-1)
            nbr = len(list_ids)
            print("Actuellement {} tweets dans la BDD".format(nbr))
            print("Le min id est : {}".format(id_max))
            public_tweets = api.home_timeline()
            search_tweets = tweepy.Cursor(api.search, q='(lang:en OR lang:fr) AND godzilla', since="2019-05-29", max_id=id_max, tweet_mode='extended').items(1000)
            for tweet in search_tweets:
                #print(tweet.id)
                if tweet.id not in list_ids:
                    tweets.append(tweet._json)
                    nbr+=1
                    print(nbr)
                else:
                    print("Already here")
    except:
        isApiOK = False


    with open('databases/bdd_extended.json', 'w+') as f:
        f.write(json.dumps(tweets))
