import matplotlib.pyplot as plt
import json
from collections import Counter
import re
import numpy as np

filter_standard_EN = ["the","I",'RT',"up","of",'was','is','are','at','a','and','movie','in','to','it','s','so','via','for','this','on','t','that','with']
filter_standard_FR = ["de",'est','la','et','était','le','les','il','au','qui','a','tu','où','sont']
filter_theme = ["Godzilla","king","monster","monsters",'2']

cntFR = Counter()
cntEN = Counter()

with open("databases/bdd_red.json",'r') as f:
    data = json.loads(f.read())
    txtFR=""
    txtEN=""
    for id,content in data.items():
        if content["lang"]=="fr":
            txtFR+=content['text']+" "
        elif content["lang"]=="en":
            txtEN+=content['text']+" "
    txtFR = txtFR.lower()
    txtEN = txtEN.lower()
    cntFR = Counter(re.findall(r'\w+', txtFR))
    cntEN = Counter(re.findall(r'\w+', txtEN))
    for word in filter_standard_EN+filter_theme:
        del(cntEN[word.lower()])
    for word in filter_standard_FR+filter_theme:
        del(cntFR[word.lower()])

plt.figure()
## Pour l'anglais
plt.subplot(121)
communsEN = cntEN.most_common(25)
print(communsEN)
wordsEN = [x[0] for x in communsEN]
valuesEN = np.arange(len(wordsEN))
countEN = [x[1] for x in communsEN]

plt.barh(valuesEN, countEN, align='center', alpha=0.5)
plt.yticks(valuesEN, wordsEN)
plt.ylabel('Nombre')
plt.title('Anglais')

## Pour le français
plt.subplot(122)
communsFR = cntFR.most_common(25)
print(communsFR)
wordsFR = [x[0] for x in communsFR]
valuesFR = np.arange(len(wordsFR))
countFR = [x[1] for x in communsFR]

plt.barh(valuesFR, countFR, align='center', alpha=0.5)
plt.yticks(valuesFR, wordsFR)
plt.ylabel('Nombre')
plt.title('Français')

plt.show()
