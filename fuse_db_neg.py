import json

files = []
files.append("databases/bdd1_red_neg.json")
files.append("databases/bdd2_red_neg.json")


tweets = []
for filename in files:
    with open(filename,'r+') as f:
        tweets.append(json.loads(f.read()))

while len(tweets) > 1:
    for tweet in tweets[1]:
        if tweet not in tweets[0]:
            tweets[0][tweet] = tweets[1][tweet]
    del tweets[1]

tweets = tweets[0]

# print(tweets)

neg_tweets = {}

for id in tweets.keys():
    if tweets[id]["note"]<0:
        neg_tweets[id]=tweets[id]

with open('databases/bdd_apprentissage.json','r+') as f:
    already_there = json.loads(f.read())

for tweet in tweets:
    if tweet not in already_there:
        already_there[tweet] = tweets[tweet]

with open('databases/bdd_apprentissage_with_more_neg.json','w+') as f:
    f.write(json.dumps(already_there))

