from textblob import TextBlob, Word, Blobber
import json
from matplotlib import pyplot as plt

db = "databases/bdd_red.json"

tweets_sentiments = {}

#On génère les données
with open(db, 'r+') as f:
    tweets = json.loads(f.read())
    for tweet in list(tweets.values()):
        if tweet['lang'] == "en": #Langue. Fr ou En.

            texte = tweet['text']
            tweet_sentiment = (TextBlob(texte)).sentiment

            if(tweet_sentiment.polarity != 0 or tweet_sentiment.subjectivity != 0): # On évite les tweets neutres
                
                tweets_sentiments[tweet['id']] = {'text': texte, 'polarity': tweet_sentiment.polarity, 'subjectivity': tweet_sentiment.subjectivity, 'rt': tweet['retweet_count'], 'fav': tweet['favorite_count']}


#Les moyennes :)  

pond_fav = 0
pond_rt = 1

def moyennes(tweets_sentiments, pond_fav, pond_rt):

    pol = 0
    sub = 0
    count = 0

    for tweet in tweets_sentiments.values():
        pol += float(tweet['polarity'])*(1+pond_rt*tweet['rt']+pond_fav*tweet['fav'])
        sub += float(tweet['subjectivity'])*(1+pond_rt*tweet['rt']+pond_fav*tweet['fav'])
        count += (1+pond_rt*tweet['rt']+pond_fav*tweet['fav'])

    return ((pol/count)/2+0.5,sub/count)

print(moyennes(tweets_sentiments, pond_fav, pond_rt))



#Affichage
def moyennes_1(tweets_sentiments, pond_fav):
    return moyennes(tweets_sentiments, pond_fav, 0)[0]

def moyennes_2(tweets_sentiments, pond_rt):
    return moyennes(tweets_sentiments, 0, pond_rt)[0]

X = [i for i in range(10)]
Y1 = [moyennes_1(tweets_sentiments,i) for i in X]
Y2 = [moyennes_2(tweets_sentiments,i) for i in X]

plt.ylim(0,1)
plt.plot(X,Y1)
#plt.show()
plt.ylim(0,1)
plt.plot(X,Y2)
#plt.show()