import numpy as np
import json
import nltk
import re
nltk.download("popular")

from nltk.corpus import stopwords
from nltk.tokenize.casual import TweetTokenizer

## On va s'occuper des tweets anglais seulement

stop_words = set(stopwords.words('english'))
stop_words_theme = ["Godzilla"]
for i,x in enumerate(stop_words_theme):
    stop_words_theme[i]=x.lower()

data = {}
index_inverse = {}



def change_token(token):
    token = token.lower()
    if len(token)==0: return None
    if re.match(r'\W', token,re.IGNORECASE)!=None: return None
    if token[0]=="#":
        token=token[1:]
    elif token[0]=="@":
        token=None
    return token

def inverse_index_creation(input_filename,output_filename):
    with open(input_filename,"r") as f:
        data = json.loads(f.read())

    for id,content in data.items():
        # On garde que les tweets en anglais
        if content['lang']!="en": continue

        # On tokenize
        tokens = TweetTokenizer().tokenize(content['text'])

        new_tokens = []
        for word in tokens:
            word = change_token(word)
            if word == None: continue
            if word not in stop_words and word not in stop_words_theme:
                new_tokens.append(word)
        data[id]['text'] = new_tokens


        # On remplit l'index inversé
        for word in new_tokens:
            ft = new_tokens.count(word)
            if word in index_inverse.keys():
                index_inverse[word].append((id,ft))
            else:
                index_inverse[word]=[(id,ft)]


    with open(output_filename,"w") as f:
        texte = json.dumps(index_inverse,indent=4)
        f.write(texte)


